<?php



class Controller
{

    public function saveProduct()
    {
        if ($_REQUEST['Type'] == "1") {
            $data = array(
                'SKU' => $_REQUEST['SKU'],
                'Name' => $_REQUEST['Name'],
                'Price' => $_REQUEST['Price'],
                'Type' => $_REQUEST['Type'],
                'Size' => $_REQUEST['Size']);

            $this->isValidProduct();

            $dbConnection = new Database();
            $dbConnection->connect();

            $add = new Model();
            $add->saveToDb('productList', $data);




        } elseif ($_REQUEST['Type'] == "2") {
            $data = array(
                'SKU' => $_REQUEST['SKU'],
                'Name' => $_REQUEST['Name'],
                'Price' => $_REQUEST['Price'],
                'Type' => $_REQUEST['Type'],
                'Height' => $_REQUEST['Height'],
                'Width' => $_REQUEST['Width'],
                'Length' => $_REQUEST['Length']);

            $this->isValidProduct();

            $dbConnection = new Database();
            $dbConnection->connect();

            $add = new Model();
            $add->saveToDb('productList', $data);

        } else {
            $data = array(
                'SKU' => $_REQUEST['SKU'],
                'Name' => $_REQUEST['Name'],
                'Price' => $_REQUEST['Price'],
                'Type' => $_REQUEST['Type'],
                'Weight' => $_REQUEST['Weight']);

            $this->isValidProduct();

            $dbConnection = new Database();
            $dbConnection->connect();

            $add = new Model();
            $add->saveToDb('productList', $data);
        }
    }

    private function isValidProduct()
    {
        if (!empty($_REQUEST['SKU']) &&
            !empty($_REQUEST['Name']) &&
            !empty($_REQUEST['Price']) &&
            !empty($_REQUEST['Type'])
        ) {
            return true;
        }
        return false;
    }

    public function showProductList()
    {

        $select = new Model();
        $products = $select->selectAllProducts();

        $g = $products;


        require_once(__ROOT__.'/views/ProductList.php');

    }
    public function deleteProduct()
    {
        $del = new Model();
        $products = $del->deleteP();

        $g = $products;

        require_once(__ROOT__.'/views/ProductList.php');
    }

}
