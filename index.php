<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

include "core/Route.php";
include "core/Database.php";

include "views/Add.php";
include "controllers/Products.php";
//include "views/ProductList.php";
//include "controllers/Controller.php";
//include "controllers/save.php";
//include "models/Model.php";


$route = new Route();

$route->add('/views/Add', 'Add');
//$route->add('/views/ProductList', 'ProductList');
$route->add('/controllers/Products', 'Products');
//$route->add('/controllers/ControllerSave', 'ControllerSave');
//$route->add('/controllers/save', 'save');
//$route->add('/models/Model', 'Model');

$route->submit();


