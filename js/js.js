$(function ()
{
    $('jqueryOptions').hide();

    $('#dropType').change(function ()
    {
        $('.jqueryOptions').slideUp();
        $('.jqueryOptions').removeClass('current-');
        $("." + $(this).val()).slideDown();
        $("." + $(this).val()).addClass('current-');
    });
});
