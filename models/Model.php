<?php

class Model extends Database
{
    public function saveToDb ($table, array $params)
    {

        $columns = implode(',', array_keys($params));

        $values = "";

        foreach ($params as $value) {
            $values .= "'{$value}',";
        }

        $values = substr($values, 0, -1);

        $sql = "INSERT INTO $table ({$columns}) VALUES ({$values})";

        $dbConnection = new Database();
        $db = $dbConnection->connect();

        $stmt = $db->prepare($sql);
        $stmt->execute(array_values($columns));



    }

    public function selectAllProducts()
    {
        $dbConnection = new Database();
        $db = $dbConnection->connect();
        $sql = "SELECT * FROM `productList`";

        $products = array();

        foreach ($db->query($sql) as $row) {

            $id = $row['id'];

            print   "<div class='product'>
                       <input name='id[]' type='checkbox' class='checkbox'  id='selectProduct' value='".$id."'>
                        <label for = 'selectProduct'>
                        <p class='SKU'>" .$row['SKU']. "</p>
                        <p class='Name'>" .$row['Name']. "</p>
                        <p class='Price'>" .$row['Price']. " $</p>";

            if ($row['Size']!='')
            {
                print    "<p class='Size'>Size " .$row['Size']. " MB</p>";

            }elseif ($row['Weight'] !='')
            {
                print "<p class='Weight'>Weight " .$row['Weight']. " KG</p>";

            }else{

                print "<p class='HxWxL'>Dimension " .$row['Height']."x" .$row['Width']. "x" .$row['Length']. "</p>";

            }
            print "</label>";
            print "</div>";
        }

        return $products;
    }

    public function deleteP()
    {
        $dbConnection = new Database();
        $db = $dbConnection->connect();

        $delete = $_POST['id'];

        $N = count($delete);
        for ($i=0; $i<$N; $i++)
        {
            $sql = $db->prepare("DELETE FROM `productList` WHERE id= :mid");
            $sql->bindParam(':mid', $delete[$i]);
            $sql->execute();
        }



    }

}