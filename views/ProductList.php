
<!DOCTYPE html>
<html lang="en">
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="../js/js.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/style.css">
        <meta charset="UTF-8">
        <title>Product list</title>
    </head>
    <body>
        <h1>Product List
            <form action='../controllers/delete.php' method='POST' class="actions">
                <select id="dropType" name = "Type">
                    <option name="delete" value="1">mass delete action</option>
                    <option name="add" value="2">add product</option>
                </select>
                <section class="jqueryOptions 1" style="display: none">
                    <input name="id" type="submit" id="ad" value="Delete">
                </section>
                <section class="jqueryOptions 2" style="display: none">
                    <a href="../index.php" name="id" type="submit" id="ad" value="Add" >Add</a>
                </section>
            </form>
        </h1>


        <div class="productList">

                <?php

                    require_once(__ROOT__.'/core/Database.php');
                    require_once(__ROOT__.'/controllers/Controller.php');

                        $g = new Controller();
                        $g->showProductList();
                ?>


</div>
</body>
</html>