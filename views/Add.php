
<!DOCTYPE html>
<html lang="en">
    <head>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="../js/js.js"></script>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <title>Product add</title>
    </head>
    <body class="bodyAdd">
    <div class="productSave">
    <h1>Product Add</h1>

        <form action="../controllers/save.php" method="GET">

            SKU <input type="text" name="SKU"><br>
            Name <input type="text" name="Name"><br>
            Price <input type="text" name="Price"><br>

            Type Switcher
            <select id="dropType" name = "Type">
                    <option value="type0">Type Switcher</option>
                    <option name = "type1" value="1">Size</option>
                    <option name = "type2" value="2">HxWxL</option>
                    <option name = "type3" value="3">Weight</option>
            </select>

            <div id="typeSelect">

                <section class="jqueryOptions 1" style="display: none">
                    <div id="type1" class="block">
                        Size <input type="number" name="Size"><br>
                        <span class="tooltiptext">Please, provide size in MB.</span>
                    </div>
                </section>

                <section class="jqueryOptions 2" style="display: none">
                    <div id="type2" class="block" >
                        Height <input type="number" name="Height"><br>
                        Width <input type="number" name="Width"><br>
                        Length <input type="number" name="Length"><br>
                        <span class="tooltiptext">Please provide dimensions.</span>
                    </div>
                </section>

                <section class="jqueryOptions 3" style="display: none">
                    <div id="type3" class="block" >
                        Weight <input type="number" name="Weight"><br>
                        <span class="tooltiptext">Please provide weight in kg.</span>
                    </div>
                </section>

            </div>

            <input class="savebtn" type="submit" value="Save">
        </form>
    </div>
    </body>
</html>
